/*******************************************************************************
* Redbone: Thread handler.
* Copyright (C) 2017 - 2025 doofus Inc. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in
*    the documentation and/or other materials provided with the
*    distribution.
* 3. Neither the name Redbone nor the names of its contributors may be
*    used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
* COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
* OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
* AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
* LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
* ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
* 
* Contributor: Brian doofus
* email: hardik.mad@gmail.com
* web:   www.tachymoron.wordpress.com
* git:   https://bitbucket.org/pundoobvi/
*
*******************************************************************************/

#include "stdio.h"

#include "../api/redbone_api.h"
#include "../inc/redbone_sm.h"
#include "../inc/redbone.h"


// uncomment to enable debug info
#define __ENABLE_DEBUG_INFO


// thread 1 vars
typedef struct
{
	// angle in degrees
	float anglex, angley, anglez;
	float setpointx, setpointy, setpointz;
	float last_errx,last_erry, last_errz;
	uint32_t pwm1, pwm2, pwm3, pwm4;
	float integral_sumx,integral_sumy,integral_sumz;
	
}dummy_thread1_var_t;

typedef struct
{

	float kp;
	float ki;
	float kd;
	// feed forward
	float kff;
	float dt;

}dummy_thread1_config_t;

dummy_thread1_config_t thread1_config = 
{
	//kp
	2.3,
	//ki
	0.5,
	//kd
	6.4,
	//ff
	2.0,
	//dt
	0.01,
};

return_status_e dummy_thread1_init();
return_status_e dummy_thread1_get_data();
return_status_e dummy_thread1_main();
//-x-x-x-x-x thread 1 vars

// thread 2 vars
typedef struct
{
	uint32_t jack;
	
}dummy_thread_2_var_t;

typedef struct
{
	uint32_t sparrow;

}dummy_thread2_config_t;

dummy_thread2_config_t thread2_config =
{
	23U,
};

return_status_e dummy_thread2_init();
return_status_e dummy_thread2_get_data();
return_status_e dummy_thread2_main();

void redbone_debug();

int main()
{
	/*!> Local Var */
	return_status_e status = RETURN_STATUS_SUCCESS;
	thread_id_t return_thread_id;
	/*!> Validation */

	/*!> Code */
	if(RETURN_STATUS_SUCCESS == status)
	{
		status = redbone_init();
	}
	if(RETURN_STATUS_SUCCESS == status)
	{
		return_thread_id = redbone_thread_add(sizeof(dummy_thread1_var_t),
																					THREAD_PRIORITY_TWO,
																					&thread1_config,
																					&dummy_thread1_init,
																					&dummy_thread1_get_data,
																					&dummy_thread1_main,
																					2);

		return_thread_id = redbone_thread_add(sizeof(dummy_thread_2_var_t),
																					THREAD_PRIORITY_ZERO, 
																					&thread2_config,
																					&dummy_thread2_init,
																					&dummy_thread2_get_data,
																					&dummy_thread2_main,
																					2);
	}
	

	// calls Redbone Main, which then calls threads Get data and Thread main from priority queue
	redbone_main();

	// display debug information
 redbone_debug();

	/*!> Return */
	printf("\n");
	return status;
}

return_status_e dummy_thread1_init()
{
	printf("\n\nThread 1 init called");
	return (RETURN_STATUS_SUCCESS); 
}

// currently directly accesses redbone_sm, should be done with pointer to pointer later
return_status_e dummy_thread1_get_data()
{
	printf("\n\nThread 1 Get data called");
	return_status_e status = RETURN_STATUS_SUCCESS;
	
	sem_req_res_access(1, 1);
	
	if(NULL != redbone_sm->thread_queue_head_ptr->resource_access_ptr)
	{
		// update Thread 1 data from sensor
		( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->resource_access_ptr)->anglex = 2.3;
		( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->resource_access_ptr)->angley = 3.3;
		( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->resource_access_ptr)->anglez = 4.3;

		( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->resource_access_ptr)->setpointx = 4.6;
		( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->resource_access_ptr)->setpointx = 3.6;
		( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->resource_access_ptr)->setpointx = 2.8;
	}
	else
	{
		status = RETURN_STATUS_FAILURE;
	}
	
	sem_rel_res_access(1, 1);

	return (status);
}

return_status_e dummy_thread1_main()
{
	printf("\n\nThread 1 MAIN called");
	return_status_e status = RETURN_STATUS_SUCCESS;
	sem_req_res_access(1, 1);
	float errx, erry, errz;
	float dt;

	if(NULL != redbone_sm->thread_queue_head_ptr->resource_access_ptr)
	{
		errx = ( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->resource_access_ptr)->setpointx - ( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->resource_access_ptr)->anglex;
		erry = ( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->resource_access_ptr)->setpointy - ( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->resource_access_ptr)->angley;
		errz = ( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->resource_access_ptr)->setpointz - ( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->resource_access_ptr)->anglez;

		dt = ( (dummy_thread1_config_t *)redbone_sm->thread_queue_head_ptr->thread_config_params_ptr)->dt;
		printf("\ndt %f",dt);

		( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->resource_access_ptr)->integral_sumx += ( (dummy_thread1_config_t *)redbone_sm->thread_queue_head_ptr->thread_config_params_ptr)->ki * (errx * dt); 
		( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->resource_access_ptr)->pwm1 = ( (dummy_thread1_config_t *)redbone_sm->thread_queue_head_ptr->thread_config_params_ptr)->kp * errx 
																																															+ ( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->resource_access_ptr)->integral_sumx
																																															+ ( (dummy_thread1_config_t *)redbone_sm->thread_queue_head_ptr->thread_config_params_ptr)->kd * ( (errx - ( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->resource_access_ptr)->last_errx)/dt);
		( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->resource_access_ptr)->pwm2 = 3.6;
		( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->resource_access_ptr)->pwm3 = 2.8;
		( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->resource_access_ptr)->pwm4 = 2.8;


		//update last error
		( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->resource_access_ptr)->last_errx = errx;
		( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->resource_access_ptr)->last_erry = erry;
		( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->resource_access_ptr)->last_errz = errz;
	}

	sem_rel_res_access(1, 1);
	return (status);
}


void redbone_debug()
{
	printf("\nREDBONE DEBUG");
	printf("\nREDBONE COnfig\n Threads upper limit: %u", redbone_sm->redbone_config.threads_upper_limit);
	printf("\n Number of threads %u", redbone_sm->thread_count);

	if(NULL != redbone_sm->thread_queue_head_ptr)
	{
		printf("\nFirst thread info\n Thread_id: %d\n SEM config %u\n SEM keys: %u", redbone_sm->thread_queue_head_ptr->thread_id, 
																																							 redbone_sm->thread_queue_head_ptr->sem_config.number_of_keys,
																																							 redbone_sm->thread_queue_head_ptr->sem.keys);
 
		printf("\nTesting thread var");
		printf("\n Thread 1\n  Anglex: %f\n  Angley: %f\n  Anglez: %f",( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->thread_var_ptr)->anglex,
																																		( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->thread_var_ptr)->angley,
																																		( ( dummy_thread1_var_t *)redbone_sm->thread_queue_head_ptr->thread_var_ptr)->anglez);
	}
	else
	{
		printf("\nQueue is empty");
	}
}

return_status_e dummy_thread2_init()
{
	printf("\nThread 2 Init Called");

	return(RETURN_STATUS_SUCCESS);
}
return_status_e dummy_thread2_get_data()
{
	printf("\nThread 2 Get data Called");

	return(RETURN_STATUS_SUCCESS);
}
return_status_e dummy_thread2_main()
{
	printf("\nThread 2 Main Called");

	return(RETURN_STATUS_SUCCESS);
}

/* END OF FILE */
