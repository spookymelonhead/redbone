/*******************************************************************************
* Redbone: Thread handler.
* Copyright (C) 2017 - 2025 doofus Inc. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in
*    the documentation and/or other materials provided with the
*    distribution.
* 3. Neither the name Redbone nor the names of its contributors may be
*    used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
* COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
* OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
* AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
* LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
* ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
* 
* Contributor: Brian doofus
* email: hardik.mad@gmail.com
* web:   www.tachymoron.wordpress.com
* git:   https://bitbucket.org/pundoobvi/
*
*******************************************************************************/

#pragma once

#include "stdint.h"
#include "stdbool.h"
#include "stdio.h"

/*----------------------------------------------------------------------------*/
/*!@brief macro to define NULL
  !@details SAFETYMCUSW 218 S MR:20.2 <APPROVED> "Custom Type Definition." */
#ifndef NULL
  #define NULL (0U)
#endif /* NULL */

/*----------------------------------------------------------------------------*/
/*!@brief macro to define NULL Pointer */
#ifndef NULL_PTR
  #define NULL_PTR ((void *)0x0U)
#endif /* NULL_PTR */

/*----------------------------------------------------------------------------*/
/*!@brief macro to define TRUE */
#ifndef TRUE
  #define TRUE true
#endif /* TRUE */

/*----------------------------------------------------------------------------*/
/*!@brief macro to define FALSE */
#ifndef FALSE
  #define FALSE false
#endif /* FALSE */

/*----------------------------------------------------------------------------*/
/*!@brief macro to define ENABLE */
#ifndef ENABLE
  #define ENABLE 1U
#endif /* ENABLE */

/*----------------------------------------------------------------------------*/
/*!@brief macro to define DISABLE */
#ifndef DISABLE
  #define DISABLE 0U
#endif /* DISABLE */

/*----------------------------------------------------------------------------*/
/*!@brief macro to define TRUE */
#ifndef TRUE
  #define TRUE true
#endif /* TRUE */

/*!> Redbone thread if typedef */
typedef int thread_id_t;

/*----------------------------------------------------------------------------*/
/*!@brief enum to define the status return type */
typedef enum
{
	RETURN_STATUS_SUCCESS											= 0U,
	RETURN_STATUS_FAILURE,

  RETURN_STATUS_INVALID_SEQUENCE,
  RETURN_STATUS_INVALID_ARGUMENT,
  RETURN_STATUS_NULL_POINTER,
  RETURN_STATUS_MEMORY_ALLOCATION_FAILED,

  RETURN_STATUS_FUNCTIONALITY_NOT_SUPPORTED,

  RETURN_STATUS_INVALID_CONFIG_PARAMETER,

  RETURN_STATUS_INVALID,
  RETURN_STATUS_NUM                         = RETURN_STATUS_INVALID
	
}return_status_e;

/*!> Redbone thread function pointer typedef definition */
typedef return_status_e ( * thread_fptr_t ) (void);

/*----------------------------------------------------------------------------*/
/*!@brief enum for thread prioirty */
typedef enum
{
	THREAD_PRIORITY_ZERO   								= 0U,
	THREAD_PRIORITY_ONE,
	THREAD_PRIORITY_TWO,

	THREAD_PRIORITY_INVALID,
	THREAD_PRIORITY_NUM										= THREAD_PRIORITY_INVALID
	
}thread_priority_e;

return_status_e redbone_init();

thread_id_t redbone_thread_add(uint32_t heap_size,
                               thread_priority_e priority,
                               void * thread_config_param_ptr,
                               thread_fptr_t thread_init_fptr,
                               thread_fptr_t thread_get_data_fptr,
                               thread_fptr_t thread_main_fptr,
                               uint32_t number_of_keys);

// free thead
return_status_e redbone_thread_remove(uint32_t thread_id);

// semaphore request resource access
thread_id_t sem_req_res_access(thread_id_t thread_id, thread_id_t target_thread_id);

// semaphore release resource access
thread_id_t sem_rel_res_access(thread_id_t thread_id, thread_id_t target_thread_id);

// thread main
return_status_e redbone_main();

/* END OF FILE */