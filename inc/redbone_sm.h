/*******************************************************************************
* Redbone: Thread handler.
* Copyright (C) 2017 - 2025 doofus Inc. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in
*    the documentation and/or other materials provided with the
*    distribution.
* 3. Neither the name Redbone nor the names of its contributors may be
*    used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
* COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
* OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
* AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
* LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
* ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
* 
* Contributor: Brian doofus
* email: hardik.mad@gmail.com
* web:   www.tachymoron.wordpress.com
* git:   https://bitbucket.org/pundoobvi/
*
*******************************************************************************/

#pragma once

#include "../api/redbone_api.h"
#include "../api/redbone_config.h"

/*----------------------------------------------------------------------------*/
/*!@brief enum to define the SM State */
typedef enum
{
	SM_STATE_DANGLING								= 0U,
	SM_STATE_ALLOCATED,
	SM_STATE_INITIALZED_TO_DEFAULT  = SM_STATE_ALLOCATED,

	SM_STATE_INVALID,
	SM_STATE_NUM										= SM_STATE_INVALID
	
}sm_state_e;

typedef enum
{
	THREAD_STATE_DANGLING									= 0U,
	THREAD_STATE_INITIALIZED_TO_DEFAULT,
	THREAD_STATE_ADDED_TO_QUEUE,
	THREAD_STATE_INITIALIZED,
	THREAD_STATE_READY                                      = THREAD_STATE_INITIALIZED,
	THREAD_STATE_GET_DATA,
	THREAD_STATE_RUN_MAIN,
	THREAD_STATE_SUSPENDED,

	THREAD_STATE_INVALID,
	THREAD_STATE_NUM									    = THREAD_STATE_INVALID
	
}thread_state_e;

typedef struct
{
	/*!> State of Redbone sm */
	sm_state_e state;

	/*!> State of Redbone sm */
	uint32_t thread_count;

	/*!> Redbone Config */
	redbone_config_s redbone_config;

	/*!> Thread Queue */
	struct thread_s * thread_queue_head_ptr;

}redbone_sm_s;

// Instance for Redbone Sm
extern redbone_sm_s * redbone_sm;

return_status_e redbone_sm_init(void);

/* END OF FILE */
