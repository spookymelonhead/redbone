/*******************************************************************************
* Redbone: Thread handler.
* Copyright (C) 2017 - 2025 doofus Inc. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in
*    the documentation and/or other materials provided with the
*    distribution.
* 3. Neither the name Redbone nor the names of its contributors may be
*    used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
* COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
* OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
* AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
* LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
* ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
* 
* Contributor: Brian doofus
* email: hardik.mad@gmail.com
* web:   www.tachymoron.wordpress.com
* git:   https://bitbucket.org/pundoobvi/
*
*******************************************************************************/

#include "stdlib.h"
#include "string.h"
#include "../api/redbone_api.h"
#include "../api/redbone_config.h"
#include "../inc/redbone_sm.h"


redbone_sm_s * redbone_sm = NULL;

return_status_e redbone_sm_init()
{
	/*!> Local Var */
	return_status_e status = RETURN_STATUS_SUCCESS;

	/*!> Validation */
	if(NULL != redbone_sm)
	{
		status = RETURN_STATUS_INVALID_SEQUENCE;
		printf("REDBONE_SM already allocated %x", status);
		return (status);
	}

	/*!> Code */
	if(RETURN_STATUS_SUCCESS == status)
	{
		redbone_sm = ( redbone_sm_s * ) malloc( sizeof( redbone_sm_s ) );

		//check if memory allocation was successfull
		if(NULL == redbone_sm)
		{
			status = RETURN_STATUS_MEMORY_ALLOCATION_FAILED;
			printf("\nRedbone init failed %x", status);
			redbone_sm = NULL;
			return (status);
		}

		else
		{
			memset( (void * ) redbone_sm, 0x0, sizeof( redbone_sm_s ) );

			redbone_sm->thread_count = 0x0U;
			redbone_sm->thread_queue_head_ptr = NULL;
			redbone_sm->redbone_config = redbone_config;

			redbone_sm->state = SM_STATE_INITIALZED_TO_DEFAULT;
		}
	}

	/*!> Return */
	return (status);
}

/* END OF FILE */
