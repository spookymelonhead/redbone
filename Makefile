#Makefile to build the Redbone 

CC = gcc

CFLAGS = -c -Wall

#This is very simple version of makefile without any variables

all: main.o
	$(CC) redbone_config.o redbone_sm.o redbone.o redbone_queue.o redbone_util.o main.o -o app

main.o: redbone_config.o redbone_sm.o redbone.o redbone_queue.o redbone_util.o
	$(CC) $(CFLAGS) -Iapi/ -Iinc/ main.c

redbone_config.o: src/redbone_config.c
	$(CC) $(CFLAGS) -Iapi/ src/redbone_config.c

redbone_sm.o: src/redbone_sm.c
	$(CC) $(CFLAGS) -Iapi/ -Iinc/ src/redbone_sm.c

redbone.o: src/redbone.c
	$(CC) $(CFLAGS) -Iapi/ -Iinc/ src/redbone.c

redbone_queue.o: src/redbone_queue.c
	$(CC) $(CFLAGS) -Iapi/ -Iinc/ src/redbone_queue.c	

redbone_util.o: src/redbone_util.c
	$(CC) $(CFLAGS) -Iapi/ -Iinc/ src/redbone_util.c	


clean:
	rm -rf bin/ *o main