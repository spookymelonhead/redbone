/*******************************************************************************
* Redbone: Thread handler.
* Copyright (C) 2017 - 2025 doofus Inc. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in
*    the documentation and/or other materials provided with the
*    distribution.
* 3. Neither the name Redbone nor the names of its contributors may be
*    used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
* COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
* OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
* AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
* LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
* ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
* 
* Contributor: Brian doofus
* email: hardik.mad@gmail.com
* web:   www.tachymoron.wordpress.com
* git:   https://bitbucket.org/pundoobvi/
*
*******************************************************************************/

#pragma once

#include "../api/redbone_api.h"
#include "redbone_sm.h"

typedef struct
{
	// present number of keys
	uint32_t keys;

	// what all threads currently can access the resource
	thread_id_t * thread_register;

}sem_t;

typedef struct
{
  // maximum number of simultaneous access, sem keys
  uint32_t number_of_keys;
  
}sem_config_t;

struct thread_s
{
	/*!> Thread State */
	thread_state_e state;

	/*!> Unique ID for thread */
	thread_id_t thread_id;

	/*!> Threads Priority */
	thread_priority_e priority;

	/*!> void pointer for thread's variables */
	void * thread_var_ptr;

	/*!> thread config params */
	void * thread_config_params_ptr;

	/*!> Resource access pointer */
	void * resource_access_ptr;

	/*!> Sem config */
	sem_config_t sem_config;

	/*!> Sempaphore to handle thread resource */
	sem_t sem;

	/*!> function pointer to init thread */
	thread_fptr_t thread_init_fptr;

	/*!> function pointer to get thread data */
	thread_fptr_t thread_get_data_fptr;

	/*!> function pointer for thread's main task i.e doing PID's and all */
	thread_fptr_t thread_main_fptr;

	/*!> Next thread */
	struct thread_s * next_thread_ptr;
	
};

// should never be called
return_status_e phantom();


/* END OF FILE */
