/*******************************************************************************
* Redbone: Thread handler.
* Copyright (C) 2017 - 2025 doofus Inc. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in
*    the documentation and/or other materials provided with the
*    distribution.
* 3. Neither the name Redbone nor the names of its contributors may be
*    used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
* COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
* OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
* AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
* LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
* ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
* 
* Contributor: Brian doofus
* email: hardik.mad@gmail.com
* web:   www.tachymoron.wordpress.com
* git:   https://bitbucket.org/pundoobvi/
*
*******************************************************************************/

#include "../api/redbone_api.h"
#include "../api/redbone_config.h"
#include "../inc/redbone_queue.h"
#include "../inc/redbone_sm.h"
#include "../inc/redbone.h"


return_status_e redbone_add_thread_to_queue(struct thread_s ** new_thread_dptr)
{
	/* Local Var */
	return_status_e status = RETURN_STATUS_SUCCESS;
	struct thread_s * traverser_thread = NULL;
	struct thread_s * previous_traverser_thread = NULL;
	struct thread_s * psuedo_thread = NULL;
  thread_priority_e thread_priority = THREAD_PRIORITY_INVALID;

	/* Validation */
	if(NULL == redbone_sm)
	{
		status = RETURN_STATUS_NULL_POINTER;
		printf("\nRedbone sm points to NULL %x", status);
		return(status);
	}

	if(NULL == new_thread_dptr)
	{
		status = RETURN_STATUS_NULL_POINTER;
		printf("\nNew Thread dptr points to NULL %x", status);
		return(status);
	}
	if(NULL == (*new_thread_dptr))
	{
		status = RETURN_STATUS_NULL_POINTER;
		printf("\n(*New Thread dptr) points to NULL %x", status);
		return(status);
	}

	if(THREAD_STATE_INITIALIZED_TO_DEFAULT != (*new_thread_dptr)->state)
	{
		status = RETURN_STATUS_NULL_POINTER;
		printf("\nRedbone sm points to NULL %x", status);
		return(status);
	}

	/* Code */
	if(RETURN_STATUS_SUCCESS == status)
	{
		thread_priority = (*new_thread_dptr)->priority;
    // queue traverser points to queue head, highest priority thread
    traverser_thread = redbone_sm->thread_queue_head_ptr;
    // previous queue traverser to NULL
    previous_traverser_thread = NULL;

    if( NULL != redbone_sm->thread_queue_head_ptr)
    {
      while(NULL != traverser_thread)
      {
        if( thread_priority <= traverser_thread->priority )
        {
          // case where head has lower prioirty than new node, INSERTION AT VERY START OF THE QUEUE
          if(redbone_sm->thread_queue_head_ptr == traverser_thread)
          {
            // point new_node->next node to current head
            (*new_thread_dptr)->next_thread_ptr = redbone_sm->thread_queue_head_ptr;
            // point head of the queue to new_node
            redbone_sm->thread_queue_head_ptr = (*new_thread_dptr);
            printf("\nThread added to queue success");
            break;
          }
          // case where a node in the middle is found to be of lower prioirty, INSERTION IN MIDDLE OF THE QUEUE
          else
          {
            previous_traverser_thread->next_thread_ptr = (*new_thread_dptr);
            (*new_thread_dptr)->next_thread_ptr = traverser_thread;
            printf("\nThread added to queue success");
            break;
          }
        }
        else if( thread_priority > traverser_thread->priority )
        {
          // case where last node in the queue has higher prioirty than new node, INSERTION AT THE LAST
          if(NULL == traverser_thread->next_thread_ptr)
          {
            // end of queue is reached but queue is not full
            if(redbone_sm->thread_count < redbone_config.threads_upper_limit)
            {
              traverser_thread->next_thread_ptr = (*new_thread_dptr);
              (*new_thread_dptr)->next_thread_ptr = NULL;
              printf("\nThread added to queue success");
              break;
            }
            else
            {
              status = RETURN_STATUS_FAILURE;
              printf("Redbone QUEUE MAX NUMBER OF NODES EXCEEDED, CHECK NUMBER OF THREADS ACTUALLY IN THE QUEUE %x", status);
              return ( status );  
            }
          } 
          else
          {
        	  // do no-thing
          }
        }
        else
        {
          // do no-thing, never comes to this.
        }
        // point previous traverser to current traverser
        previous_traverser_thread = traverser_thread;
        // point traverser to next node
        if(NULL != traverser_thread->next_thread_ptr)
        {
          traverser_thread = traverser_thread->next_thread_ptr;
        }
        else
        {
          break;
        }  
      }
    }
    else
    {
      // point new_node->next_node to NULL
      (*new_thread_dptr)->next_thread_ptr = NULL;
      // point head of the queue to new_node
      redbone_sm->thread_queue_head_ptr = (*new_thread_dptr);
      printf("\nThread added to queue success");
      #warning "Brian: DONT INCRMENT THE COUNTER JUST SET IT TO ONE, ONCE Redbone STABLE"
    }

    if(RETURN_STATUS_SUCCESS == status)
    {
      // update number of nodes present in queue counter
      redbone_sm->thread_count += 1U;
      (*new_thread_dptr)->state = THREAD_STATE_ADDED_TO_QUEUE;    
    }
	}

	/* Return */
	return(status);
}

// // add thread to queue, ethically should be done in redbone_queue.c
//  if( NULL == redbone_sm->thread_queue_head_ptr)
//  {
//    redbone_sm->thread_queue_head_ptr = new_thread;
//    redbone_sm->thread_queue_head_ptr->state = THREAD_STATE_ADDED_TO_QUEUE;
//  }
//  else
//  {
//    traverser_thread = redbone_sm->thread_queue_head_ptr;
//    while(NULL != traverser_thread)
//    {
//      if(NULL != traverser_thread->next_thread_ptr)
//      {
//        traverser_thread = traverser_thread->next_thread_ptr;
//      }
//      else break;

//      traverser_thread->next_thread_ptr = new_thread;
//      traverser_thread->next_thread_ptr->state = THREAD_STATE_ADDED_TO_QUEUE;     
//    }
//  }

/* END OF FILE */