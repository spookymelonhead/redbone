* Redbone: Thread handler.
* Copyright (C) 2017 - 2025 doofus Inc. All rights reserved.

* Contributor: Brian doofus
* E-mail: hardik.mad@gmail.com
* Web: www.tachymoron.wordpress.com
* git: https://bitbucket.org/pundoobvi/
*		   https://github.com/pundoobvi/

Tree- 

├── api
│   ├── redbone_api.h
│   ├── redbone_config.h
│   └── redbone_util.h
├── inc
│   ├── redbone.h
│   ├── redbone_queue.h
│   └── redbone_sm.h
├── main.c
├── Makefile
├── README.md
└── src
    ├── redbone.c
    ├── redbone_config.c
    ├── redbone_queue.c
    ├── redbone_sm.c
    └── redbone_util.c

api/- main() can only include files from api/ NOT inc/ (If they are seen included that is for development purposes only).
inc/- src/ can include inc/.
src/- holds all the source files.

redbone_api.h
Exposes required items from redbone to the user, i.e. typedef for thread pointer, various data types, add_thread function, return type enum etc.

redbone_config.h
Config file structure definition. wanna change config?-> Open redbone_config.c.

redbone_util.h
Utilities if any(only fake delay for the idle thread for now).

redbone.h
Defines a structure(struct thread_s) for the thread's queue.

Has the below entities-
 - state: to hold the current state of the thread.
 - thread_id:
 - priority: (enum defined in redbone_api.h)
 - thread_var_ptr(void * ): Every thread has some data, put into nice little structure, redbone allocates memory for that structure 
 														and thread can access that memory via this pointer.
 - thread_config_params_ptr(void * ): Pointer to config variables(again put into a nice structure) of the thread, if any.
 - resource_access_ptr: Pointer to access other threads resource or its own (thread_var_ptr, semaphore point this pointer to the 														 desired required resource). Main() should not have access to Redbone_sm but for now it does has access. 
 - sem_config: config related to semaphore
 - sem: sem related stuff, thread_register etc.
 - thread_init_fptr: Just like variable pointers, there are function pointers, fptr for thread init.
 - thread_get_data_fptr: Thread's get data fptr.
 - thread_main_fptr: Thread main.

redbone_queue.h:
has one function to insert thread into queue, according with the thread's prioirty.

redbone_sm.h:
structure to hold data related to redbone and queue head pointer and stuff.

main():
- defines dummy structures for thread1 and thread 2.
- calls redbone_init()
- Add's thread to redbone queue, thread 1 then thread 2.
- calls redbone_main()- which traverses thread's from the queue calls threads get data and main.

thread_1_get_data():
- calls sem_req_res_access(1, 1) : which is thread 1 requesting access for thread 1 data. Which is accessible thru resource access 			pointer FOR NOW.
- typecasts the void pointer to thread1 data type, and updates values.
- sem_rel_res_access(1, 1): releases access.

thread_1_main():
- again gains access of the data using sem_req_access then does whatever you wanna do with the data.
- releases sem access.

sem_req_access(thread_id, target_thread_id):
- traverses queue
- finds both the threads
- points the resource access pointer of (thread_id) to pointer thread_var_ptr of (target_thread_id).
- makes entry in target thread id sem->thread_register that (thread_id) has access to (target thread id ) resources.
