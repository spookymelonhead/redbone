/*******************************************************************************
* Redbone: Thread handler.
* Copyright (C) 2017 - 2025 doofus Inc. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in
*    the documentation and/or other materials provided with the
*    distribution.
* 3. Neither the name Redbone nor the names of its contributors may be
*    used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
* COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
* OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
* AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
* LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
* ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
* 
* Contributor: Brian doofus
* email: hardik.mad@gmail.com
* web:   www.tachymoron.wordpress.com
* git:   https://bitbucket.org/pundoobvi/
*
*******************************************************************************/

#include "stdlib.h"
#include "string.h"
#include "../api/redbone_api.h"
#include "../api/redbone_config.h"
#include "../api/redbone_util.h"
#include "../inc/redbone_sm.h"
#include "../inc/redbone.h"
#include "../inc/redbone_queue.h"

return_status_e redbone_init()
{
	/*!> Local Var */
	return_status_e status = RETURN_STATUS_SUCCESS;

	/*!> Validation */
	if(TRUE != redbone_config.redbone_enable)
	{
		status = RETURN_STATUS_INVALID_CONFIG_PARAMETER;
		return status;
	}

	/*!> Code */
	if(RETURN_STATUS_SUCCESS == status)
	{
		status = redbone_sm_init();;
	}

	if(RETURN_STATUS_SUCCESS == status)
	{
		printf("\nRedbone Init Success");
	}

	/*!> Return */
	return ( status );	
}

thread_id_t redbone_thread_add(uint32_t heap_size,
														   thread_priority_e priority,
														   void * thread_config_params_ptr,
														   thread_fptr_t thread_init_fptr,
															 thread_fptr_t thread_get_data_fptr,
															 thread_fptr_t thread_main_fptr,
															 uint32_t number_of_keys)
{
	/*!> Local Var */
	return_status_e status = RETURN_STATUS_SUCCESS;
	thread_id_t thread_id;
	struct thread_s * new_thread = NULL;

	/*!> Validation */
	if((void *)NULL == redbone_sm)
	{
		status = RETURN_STATUS_INVALID_SEQUENCE;
		printf("\nCONNER SM POINTS TO NULL %x", status);
		return ( status );
	}
	if(redbone_sm->thread_count +1U >= redbone_sm->redbone_config.threads_upper_limit)
	{
		status = RETURN_STATUS_FAILURE; 
		printf("\nTHREAD QUEUE FULL %x", status);
		return ( status );
	}
	if(0U == heap_size)
	{
		status = RETURN_STATUS_INVALID_ARGUMENT;
		printf("\nHEAP SIZE FOR THREAD INVALID %x", status);
		return ( status );
	}
	if(NULL == thread_config_params_ptr)
	{
		status = RETURN_STATUS_INVALID_ARGUMENT;
		printf("\nConfig param ptr points to NULL %x", status);
		return ( status );		
	}
	if( (void *) NULL == thread_init_fptr)
	{
		status = RETURN_STATUS_INVALID_ARGUMENT;
		printf("\nInit FOR THREAD INVALID %x", status);
		return ( status );
	}
	if( (void *) NULL == thread_get_data_fptr)
	{
		status = RETURN_STATUS_INVALID_ARGUMENT;
		printf("\nGET DATA FOR THREAD INVALID %x", status);
		return ( status );
	}
	if( (void *) NULL == thread_main_fptr)
	{
		status = RETURN_STATUS_INVALID_ARGUMENT;
		printf("\nMAIN FOR THREAD INVALID %x", status);
		return ( status );		
	}

	/*!> Code */
	if(RETURN_STATUS_SUCCESS == status)
	{
		new_thread = ( struct thread_s * ) malloc( sizeof( struct thread_s ) );
		if( (void *) NULL == new_thread )
		{
			status = RETURN_STATUS_MEMORY_ALLOCATION_FAILED;
			printf("\nMAIN FOR THREAD INVALID %x", status);
			return ( status );					
		}
		else
		{
			memset( (void *) new_thread, 0x0, sizeof( struct thread_s ) );

			// new thread id equals thread count + 1
			new_thread->thread_id = redbone_sm->thread_count + 1U;
			thread_id = new_thread->thread_id;
			// Priority not functional yet
			new_thread->priority = priority;
			// threads data, which redbone would be completely unaware of
			new_thread->thread_var_ptr = (void *) malloc(heap_size);
			if(NULL == new_thread->thread_var_ptr)
			{
				status = RETURN_STATUS_MEMORY_ALLOCATION_FAILED;
				printf("\nMemory allocation for a pointer failed %x", status);
				return (status);
			}
			memset( (void*)new_thread->thread_var_ptr, 0x0, heap_size);

			// Thread config params, PID , fptr should not be pointed to NULL, causes device reset when called
			new_thread->thread_config_params_ptr = (void *) thread_config_params_ptr;
			// Resource access pointer
			new_thread->resource_access_ptr = (void *) NULL;
			// thread init if any
			new_thread->thread_init_fptr = thread_init_fptr;
			// thread get data, steored data in thread_var_ptr, part of application 
			new_thread->thread_get_data_fptr = thread_get_data_fptr;
			// Operates on received Data
			new_thread->thread_main_fptr = thread_main_fptr;
			// Number of Sem keys
			new_thread->sem_config.number_of_keys = number_of_keys;
			// initialize with number of keys
			new_thread->sem.keys = new_thread->sem_config.number_of_keys;
			
			new_thread->sem.thread_register = (thread_id_t *) malloc(number_of_keys * sizeof(thread_id_t) );
			if( NULL == new_thread->sem.thread_register)
			{
				status = RETURN_STATUS_MEMORY_ALLOCATION_FAILED;
				printf("\nThread register mem allocation failed %x", status);
				free(new_thread);
				return (status);
			}
			memset((void * )new_thread->sem.thread_register, 0x0, number_of_keys * sizeof(thread_id_t));
		}
	}

	if(RETURN_STATUS_SUCCESS == status)
	{
		// Thread state to Default
		new_thread->state = THREAD_STATE_INITIALIZED_TO_DEFAULT;
		status = redbone_add_thread_to_queue(&new_thread);	
	}

	/*!> Return */
	if(RETURN_STATUS_SUCCESS == status && THREAD_STATE_ADDED_TO_QUEUE == new_thread->state)
	{
		// call init function for the thread
		printf("\nCalling Thread %u Init", new_thread->thread_id);

		status = (*new_thread->thread_init_fptr)();
		if(RETURN_STATUS_SUCCESS == status)
		{
			new_thread->state = THREAD_STATE_INITIALIZED;
		}
		else
		{
			printf("Thread id %u Init Failed", new_thread->thread_id);
			new_thread->state = THREAD_STATE_SUSPENDED;
		}
		new_thread = NULL;
	}
	else
	{
		printf("\nThread %u Failed to add to queue ", thread_id);
		free(new_thread->thread_var_ptr);
		free(new_thread->thread_config_params_ptr);
		free(new_thread->sem.thread_register);
		free(new_thread);	
		return (-1);
	}
}

// semaphore request resource access
// returns thread id if Success
// -1 if fail
thread_id_t sem_req_res_access(thread_id_t thread_id, thread_id_t target_thread_id)
{
	/*!> Local Var */
	return_status_e status = RETURN_STATUS_SUCCESS;
	struct thread_s * traverser_thread  = NULL;
	struct thread_s * psuedo_thread 		= NULL;

	struct thread_s * target_thread_ptr = NULL;
	struct thread_s * thread_ptr 				= NULL;

	/*!> Validation */
	if(0U == thread_id && redbone_sm->thread_count < thread_id)
	{
		status = RETURN_STATUS_INVALID_ARGUMENT;
		printf("INVALID THREAD ID %x", status);
		return (-1);
	}
	if(0U == target_thread_id && redbone_sm->thread_count < target_thread_id)
	{
		status = RETURN_STATUS_INVALID_ARGUMENT;
		printf("INVALID TARGET THREAD ID %x", status);
		return (-1);
	}
	/*!> Code*/
	if(RETURN_STATUS_SUCCESS == status)
	{
		traverser_thread = redbone_sm->thread_queue_head_ptr;
		while(traverser_thread != NULL)
		{
			if(thread_id == traverser_thread->thread_id)
			{
				thread_ptr = traverser_thread;
				if(target_thread_id == thread_id)
				{
					target_thread_ptr = thread_ptr;
					break;
				}
			}
			else if (target_thread_id == traverser_thread->thread_id)
			{
				target_thread_ptr = traverser_thread;	
			}
			else
			{
				// do nothing
			}	
			if(NULL != traverser_thread->next_thread_ptr)
			{
				traverser_thread = traverser_thread->next_thread_ptr;
			}
			else
			{
				break;		
			}
		}
		// give access to the pointer, derement number kof keys, SEMAPHORE STUFF
		if(0 < target_thread_ptr->sem.keys && target_thread_ptr->sem.keys <= target_thread_ptr->sem_config.number_of_keys )
		{
			printf("\nThread entry made in Thread %u register at index %u for thread %u ",target_thread_id, 
																																										target_thread_ptr->sem_config.number_of_keys - target_thread_ptr->sem.keys, 
																																									  thread_id);
			// point thread resrource access pointer to target resrource
			thread_ptr->resource_access_ptr = target_thread_ptr->thread_var_ptr;
			// register thread id into target sem thread id register
			target_thread_ptr->sem.thread_register[target_thread_ptr->sem_config.number_of_keys - 
																						 target_thread_ptr->sem.keys] = thread_id;

			printf("\nThread %u Sem-1", target_thread_id);
			// decrement number of keys
			target_thread_ptr->sem.keys--;
		}

		else
		{
			return (-1);
		}
	}

	if(RETURN_STATUS_SUCCESS == status)
	{
		return(thread_id);
	}
	else
	{
		return (-1);
	}
}

// semaphore release resource access
thread_id_t sem_rel_res_access(thread_id_t thread_id, thread_id_t target_thread_id)
{
	/*!> Local Var */
	return_status_e status = RETURN_STATUS_SUCCESS;
	struct thread_s * traverser_thread  = NULL;
	struct thread_s * psuedo_thread 		= NULL;

	struct thread_s * target_thread_ptr = NULL;
	struct thread_s * thread_ptr 				= NULL;

	bool found_flag = FALSE;
	uint32_t thread_counter = 0U;

	/*!> Validation */
	if(0U == thread_id && redbone_sm->thread_count < thread_id)
	{
		status = RETURN_STATUS_INVALID_ARGUMENT;
		printf("INVALID THREAD ID %x", status);
		return (-1);
	}
	if(0U == target_thread_id && redbone_sm->thread_count < target_thread_id)
	{
		status = RETURN_STATUS_INVALID_ARGUMENT;
		printf("INVALID TARGET THREAD ID %x", status);
		return (-1);
	}
	/*!> Code*/
	if(RETURN_STATUS_SUCCESS == status)
	{
		traverser_thread = redbone_sm->thread_queue_head_ptr;
		traverser_thread = redbone_sm->thread_queue_head_ptr;
		while(traverser_thread != NULL)
		{
			if(thread_id == traverser_thread->thread_id)
			{
				thread_ptr = traverser_thread;
				if(target_thread_id == thread_id)
				{
					target_thread_ptr = thread_ptr;
					break;
				}
			}
			else if (target_thread_id == traverser_thread->thread_id)
			{
				target_thread_ptr = traverser_thread;	
			}
			else
			{
				// do nothing
			}	
			if(NULL != traverser_thread->next_thread_ptr)
			{
				traverser_thread = traverser_thread->next_thread_ptr;
			}
			else
			{
				break;		
			}
		}

		while(thread_counter != target_thread_ptr->sem_config.number_of_keys)
		{
			if(target_thread_ptr->sem.thread_register[thread_counter] == thread_id)
			{
				found_flag = TRUE;
				printf("\nThread entry found in Thread %u register at index %u for thread %u ",target_thread_id, thread_counter, thread_id);
				break;
			}
			else
			{
				thread_counter += 1U;
			}
		}

		// return if thread idin register not found
		if(false == found_flag)
		{
			return (-1);
		}

		// give access to the pointer, derement number kof keys, SEMAPHORE STUFF
		if(target_thread_ptr->sem_config.number_of_keys > target_thread_ptr->sem.keys)
		{
			printf("\nThread %u Sem+1", target_thread_id);
			// decrement number of keys
			target_thread_ptr->sem.keys++;
			// thread resrource access pointer to NULL
			thread_ptr->resource_access_ptr = NULL;
			// remove registered thread id into target sem thread id register
			target_thread_ptr->sem.thread_register[thread_counter] = 0;
		}
	}

	if(RETURN_STATUS_SUCCESS == status)
	{
		return(thread_id);
	}
	else
	{
		return (-1);
	}
}

// fptr should not be pointed to NULL, causes device reset when called
return_status_e phantom()
{
	while(1);

	return(RETURN_STATUS_SUCCESS);
}


return_status_e idle_thread()
{
	delay(10);
	return(RETURN_STATUS_SUCCESS);
}

return_status_e redbone_main()
{
	/*!> Local Var */
	return_status_e status = RETURN_STATUS_SUCCESS;

	/*!> Validation */
	if(NULL == redbone_sm)
	{
		status = RETURN_STATUS_NULL_POINTER;
		printf("Rebone sm points to NULL %x", status);
		return (status);
	}

	if(NULL == redbone_sm->thread_queue_head_ptr)
	{
		printf("Redbone thread queue empty, Idle Running");
		status = idle_thread();
		return(status);
	}

	/*!> Code */
	if(RETURN_STATUS_SUCCESS == status)
	{
		printf("\nRedbone MAIN");
		while(NULL != redbone_sm->thread_queue_head_ptr)
		{
			if(THREAD_STATE_READY == redbone_sm->thread_queue_head_ptr->state)
			{
				printf("\nCalling Thread %u Get Data", redbone_sm->thread_queue_head_ptr->thread_id);
				status = (*redbone_sm->thread_queue_head_ptr->thread_get_data_fptr)();
				if(RETURN_STATUS_SUCCESS == status)
				{
					redbone_sm->thread_queue_head_ptr->state = THREAD_STATE_GET_DATA;		
				}
			}
			else
				printf("Redbone thread id %u invalid state %x", redbone_sm->thread_queue_head_ptr->thread_id, redbone_sm->thread_queue_head_ptr->state );

			if(THREAD_STATE_GET_DATA == redbone_sm->thread_queue_head_ptr->state)
			{
				printf("\nCalling Thread %u Main", redbone_sm->thread_queue_head_ptr->thread_id);
				status = (*redbone_sm->thread_queue_head_ptr->thread_main_fptr)();
				if(RETURN_STATUS_SUCCESS == status)
				{
					redbone_sm->thread_queue_head_ptr->state = THREAD_STATE_READY;		
				}
			}
			else
				printf("Redbone thread id %u invalid state %x", redbone_sm->thread_queue_head_ptr->thread_id, redbone_sm->thread_queue_head_ptr->state );

			if(RETURN_STATUS_SUCCESS == status)
			{
				if(NULL != redbone_sm->thread_queue_head_ptr->next_thread_ptr)
				{
					redbone_sm->thread_queue_head_ptr = redbone_sm->thread_queue_head_ptr->next_thread_ptr;
				}
				else
				{
					break;
				}
			}
		}
	}
	/*!> Return */
	return (status);
}

/* END OF FILE */

